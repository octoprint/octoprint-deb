FROM debian:bullseye-backports

RUN apt update
RUN apt -y dist-upgrade

COPY $CI_PROJECT_DIR/debian/output/*.deb /root/
RUN apt -y install /root/*.deb
RUN apt -y install vim procps iproute2 less tmux sudo
# CPU* is not supported in containers
RUN grep -v '^CPU' `dpkg -L octoprint | grep octoprint.service` > /etc/systemd/system/octoprint.service
EXPOSE 5000

CMD [ "/sbin/init"]

