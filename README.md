# octoprint-deb

## Package Installation

```
sudo mkdir -p /etc/apt/local.keys.d
curl -s -o - https://octoprint.bzed.site/octoprint-deb/debian/public-key.asc | sudo bash -c 'gpg --dearmor > /etc/apt/local.keys.d/octoprint.key'
echo 'deb [arch=all signed-by=/etc/apt/local.keys.d/octoprint.key] https://octoprint.bzed.site/octoprint-deb/debian/ stable main' | sudo tee /etc/apt/sources.list.d/octoprint.list
sudo apt update && sudo apt install octoprint
```

